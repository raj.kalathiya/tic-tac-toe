import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

class Play {

    static  List<List<Character>> board = Arrays.asList(
                                                        Arrays.asList('1','2','3'),
                                                        Arrays.asList('4','5','6'),
                                                        Arrays.asList('7','8','9'));

    static int size = 3;

    public static Character makeMove(char choice, int row, int col){
        board.get(row).set(col, choice);
        if(checkWinner())
            return choice;
        return '0';
    }

    public static boolean checkStatus(List<Character> trio){
        return trio.get(0) == trio.get(1) && trio.get(1) == trio.get(2);
    }

    public static boolean checkWinner(){
        List<Character> trio = new ArrayList<>(size);
        for(int i=0; i<size; i++){
            if(checkStatus(board.get(i))){
                return true;
            }
            trio.add(board.get(i).get(i));
        }
        if(checkStatus(trio)){
            return true;
        }
        trio.clear();
        for(int i=0;i<size;i++){
            for(int j=0;j<size;j++)
                trio.add(board.get(j).get(i));
            if(checkStatus(trio)){
                return true;
            }
            trio.clear();
        }
        return checkStatus(Arrays.asList(board.get(0).get(2), board.get(1).get(1), board.get(2).get(0)));
    }

    public static void printBoard(){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<size;i++){
            for(int j=0;j<size;j++)
                sb.append(board.get(i).get(j) + "|");
            sb.append("\n______\n");
        }
        System.out.print(sb);
    }
}

public class Game {
    public static void main(String[] args) {

        String player1, player2;
        char choice;
        int cell,row,col;
        Scanner sc = new Scanner(System.in);

        List<Character> choices = Arrays.asList('X', 'O');

        System.out.print("Enter Player1 name :- ");
        player1 = sc.nextLine();
        System.out.print("Enter Player2 name :- ");
        player2 = sc.nextLine();

        do{
            System.out.print(player1 + " choose X or O :- ");
            choice = Character.toUpperCase(sc.next().charAt(0));
        }while(choice != 'X' && choice != 'O');

        choices.set(0, choice);
        if(choice != 'X'){
            choices.set(1, 'X');
        }

        Play.printBoard();

        for(int i=0; i < 9; i++) {
            //Checks for valid cell
            do {
                if (i % 2 == 0) {
                    System.out.print(player1 + "'s turn :- ");
                } else {
                    System.out.print(player2 + "'s turn :- ");
                }
                cell = sc.nextInt();
                row = (int) (Math.ceil(cell / (float)Play.size) - 1);
                col = cell % Play.size - 1;
                if(col < 0){
                    col += Play.size;
                }
                System.out.println(col);
            } while (cell < 1 || cell > 9 || Play.board.get(row).get(col) == 'X' || Play.board.get(row).get(col) == 'O');

            System.out.println();
            Character result = Play.makeMove(choices.get(i % 2), row, col);
            Play.printBoard();

            if (result != '0') {
                if (choices.indexOf(result) == 0) {
                    System.out.println(player1 + " wins the game");
                } else {
                    System.out.println(player2 + " wins the game");
                }
                System.exit(0);
            }
        }
        System.out.println("Draw!!");
    }
}